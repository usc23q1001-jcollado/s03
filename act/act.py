# LEAPYEAR

year = int(input("Please enter the  year: \n"))

if (year > 0):
	if (year % 100 == 0) and (year % 400 == 0):
	    print(f"{year} is a leap year")

	elif (year % 4 ==0) and (year % 100 != 0):
	    print(f"{year} is a leap year")

	else:
	    print(f"{year} is not a leap year")
else:
	print("year must not be 0 or negative value")

print("\n")

# ASTERISK


row = int(input("Please enter the  number of rows: \n"))
cols = int(input("Please enter the  numbe of columns: \n"))

for a in range(row):
    for b in range(cols):
        print('*',end = ' ')
    print()